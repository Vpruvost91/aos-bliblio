// The root URL for the RESTful services
var rootURL = "http://localhost:8080/BibRestPart1/webresources/bib";
var deleteURL="";
var currentBook="";

// retrouve la liste de tous les livres au demarrage 
findAll();   

$('#btnSearchID').click(function() {
	searchKey($('#searchKey').val());
	return false;
});

$('#btnSearchAuteur').click(function() {
	searchAuteur($('#searchAuteur').val());
	return false;
});

$('#btnSearchGenre').click(function() {
	searchGenre($('#searchGenre').val());
	return false;
});

$('#btnSearchTitre').click(function() {
	searchTitre($('#searchTitre').val());
	return false;
});

$('#btnDeleteID').click(function() {
	deleteLivre($('#deleteID').val());
	return false;
});

$('#btnAddAll').click(function() {
	addAll();
	return false;
});

$('#bookList a').live('click', function() {
	findById($(this).data('identity'));
});

$('#genreList').live('click', function() {
	findById($(this).data('identity'));
});

function searchKey(searchKey) {
	if (searchKey === '') 
		findAll();
	else
		findByIdentifiant(searchKey);
}

function searchAuteur(searchAuteur) {
	if (searchAuteur === '') 
		findAll();
	else
		findByAuteur(searchAuteur);
}

function searchGenre(searchGenre) {
	if (searchGenre === '') 
		findAll();
	else
		findByGenre(searchGenre);
}
     
function searchTitre(searchTitre) {
	if (searchTitre === '') 
		findAll();
	else
		findByTitre(searchTitre);
}
     
function findAll() {
    console.log('findAll');

    $.ajax({
        type: 'GET',
        url: rootURL,
        cache: false
    }).done(renderList);
}

function findByIdentifiant(searchKey) {
    console.log('findByIdentifiant: ' + searchKey);
    $.ajax({
        type: 'GET',
        url: rootURL + '/' + searchKey,
        dataType: "json"
    }).done(function (data) {
        $('#btnDelete').show();
        console.log('findById success: ' + data.titre);
        currentBook = data;
        renderDetails(currentBook);
    });

}

function findByAuteur(searchAuteur) {
    console.log('findByAuteur: ' + searchAuteur);
    $.ajax({
        type: 'GET',
        url: rootURL + '/auteur/' + searchAuteur,
        dataType: "json"
    }).done(function (data) {
        $('#btnDelete').show();
        console.log('findByAuteur success: ' + data.titre);
        currentBook = data;
        renderList(currentBook);
    });

}

function findByGenre(searchGenre) {
    console.log('findByGenre: ' + searchGenre);
    $.ajax({
        type: 'GET',
        url: rootURL + '/genre/' + searchGenre,
        dataType: "json"
    }).done(function (data) {
        $('#btnDelete').show();
        console.log('findByGenre success: ' + data.titre);
        currentBook = data;
        renderList(currentBook);
    });

}

function findByTitre(searchTitre) {
    console.log('findByTitre: ' + searchTitre);
    $.ajax({
        type: 'GET',
        url: rootURL + '/titre/' + searchTitre,
        dataType: "json"
    }).done(function (data) {
        $('#btnDelete').show();
        console.log('findByTitre success: ' + data.titre);
        currentBook = data;
        renderList(currentBook);
    });
}

function deleteLivre(idLivre) {
    console.log('deleteByID: ' + idLivre);
    $.ajax({
        type: 'DELETE',
        url: rootURL + '/' + idLivre,
        dataType: "json",
        succes : (function (data){
            $('#btnDelete').show();
            console.log('DELETED');
            alert('Livre supprimé');
        })
    });
}

function renderList(data) {
	
	$('#bookList li').remove();
	if(data!==null){
		if (data instanceof Array){
			for(key in data){
				var unLivre = data[key];
				$('#bookList').append('<li><a>'+unLivre.ISBN+' - '+unLivre.titre+' - '+unLivre.auteur+'</a></li>');
			}
		} 
		else {
			alert('Pas de livre trouvé'); 
			$('#bookList').append('<li><a>'+bib.ISBN+' - '+bib.titre+' - '+bib.auteur+'</a></li>');
		}
	}
}

function renderDetails(book) {
	$('#bookList li').remove();
	$('#bookList').append('<li><a>'+book.titre+' - '+book.auteur+'</a></li>');
}

function formToJSON() {
	return JSON.stringify({
		"titre": $('#titre').val(), 
		"auteur": $('#auteur').val()
		});
}
function livreToString() {
	return 'titre-'+$('#titre').val()+'/auteur-'+$('#auteur').val();  
}
