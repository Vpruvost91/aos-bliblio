/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio;

public class Livre {
    
        private static int isbn2 = 0;
	private String titre;
	private String auteur;
	private Genre genre;
	private int isbn;

        public enum Genre{
            AVENTURE,
            POLICIER,
            DRAME,
            DOCUMENTAIRE,
            ENFANT, 
            INCONNU
        }
        
	public Livre()
	{
		this.titre = "Inconnu";
		this.auteur= "Inconnu"; 
		this.genre = Genre.INCONNU; 
		this.isbn  = 0; 
	}
	
	public Livre(String a, String t, Genre g, int i)
	{ 
		this.auteur=a;
		this.titre=t;
                this.genre=g;
                this.isbn=i;
	}
        
        public Livre(String a, String t, Genre g)
	{ 
		this.auteur=a;
		this.titre=t;
                this.genre=g;
                this.isbn = isbn2;
                isbn2++;
	}
	 
	public String getTitre(){return titre;}
	public String getAuteur(){return auteur;}
	public Genre getGenre(){return genre;}
	public int getISBN(){return isbn;}

	public void setTitre(String t){titre=t;}
	public void setAuteur(String a){auteur=a;}
	public void setGenre(Genre g){genre=g;}
	public void setISBN(int i){isbn=i;}	
        
        public String genreToString(){
            if(genre==Genre.AVENTURE){
                return "AVENTURE";
            }
            else if(genre==Genre.POLICIER){
                return "POLICIER";
            }
            else if(genre==Genre.DRAME){
                return "DRAME";
            }
            else if(genre==Genre.DOCUMENTAIRE){
                return "DOCUMENTAIRE";
            }
            else if(genre==Genre.ENFANT){
                return "ENFANT";
            }
            return "INCONNU";
        }
        public String json(){
            return "{\"titre\": \""+titre+
                    "\", \"auteur\": \""+auteur+
                    "\", \"genre\": \""+genreToString()+
                    "\", \"ISBN\": \""+isbn+"\"}";
        }
}
