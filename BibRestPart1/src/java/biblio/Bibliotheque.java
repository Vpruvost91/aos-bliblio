/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("bib")
public class Bibliotheque 
{
    @Context
    private UriInfo context;

    private static final List<Livre> livres = new ArrayList();
    private static int compteur = 0;
    public Bibliotheque() 
    {   
        if(compteur==0){
        livres.add(new Livre("J.K. Rowling","Harry Potter a l'ecole des sorciers",Livre.Genre.ENFANT,12301));
        livres.add(new Livre("J.K. Rowling","Harry Potter et la coupe de feu",Livre.Genre.ENFANT,12302));
        livres.add(new Livre("D. Brown","Da Vinci Code",Livre.Genre.POLICIER,11244));
        livres.add(new Livre("J. Verne","20000 lieux sous les mers",Livre.Genre.AVENTURE,23900));
        }
        compteur=1;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)   
    public String getLivres() 
    {  
        return construireJSONListe(livres);
    }
 
    @GET   // this method process GET request from client for getting one book
    @Path("{uniqueId}")
    @Produces(MediaType.APPLICATION_JSON)   // sends JSON
    public String getLivreParISBN( @PathParam("uniqueId") int id)
    {  
        for(Livre l:livres){
            if(l.getISBN()==id){
                return l.json();
            }
        }
        
        Livre d = new Livre();
        return d.json();
    }   
   @DELETE   // this method process GET request from client for getting one book
    @Path("{uniqueId}")
    public void delLivreParISBN( @PathParam("uniqueId") int id)
    {  
        for(Livre l:livres){
            if(l.getISBN()==id){
                livres.remove(l);
                return;
            }
        }
        
    }   
    
    @POST
    @Path("create/{titre}/{auteur}/{genre}")
    public boolean addNewLivre( @PathParam("auteur") String auteur, @PathParam("titre") String titre, @PathParam("genre") Livre.Genre genre) 
    {  
        boolean added = false;
        
        List<Livre> liste_livres = new ArrayList();
        int liste_livres_size = liste_livres.size();
        
        liste_livres.add(new Livre(auteur,titre,genre));
        
        if(liste_livres_size < liste_livres.size()){
            added = true;
        }
        
        return added;
    }

    @GET   
    @Path("auteur/{auteur}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getLivreParAuteur( @PathParam("auteur") String a) 
    {  
        List<Livre> liste_livres = new ArrayList();
         for(Livre l:livres){
            if(l.getAuteur().contains(a)){
                liste_livres.add(l);
            }
        }
        
        return construireJSONListe(liste_livres);
    }   
    
    @GET   
    @Path("titre/{titre}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getLivreParTitre( @PathParam("titre") String a) 
    {  
        List<Livre> liste_livres = new ArrayList();
         for(Livre l:livres){
            if(l.getTitre().contains(a)){
                liste_livres.add(l);
            }
        }
        
        return construireJSONListe(liste_livres);
    }   
    
    @GET 
    @Path("find")  
    @Produces(MediaType.APPLICATION_JSON)
    public String queryLivreParAuteur(
            @QueryParam("auteur") String a, 
            @DefaultValue("") @QueryParam("dans_titre") String m) 
    {  
        List<Livre> liste_livres = new ArrayList();
         for(Livre l:livres){
            if(l.getAuteur().contains(a) && l.getTitre().contains(m)){
                liste_livres.add(l);
            }
        }
        
        return construireJSONListe(liste_livres);
    }   

    @GET   
    @Path("genre/{genre}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getLivreParGenre( @PathParam("genre") String g) 
    {  
        List<Livre> liste_livres = new ArrayList();
         for(Livre l:livres){
            if(l.genreToString().equalsIgnoreCase(g)){
                liste_livres.add(l);
            }
        }
        
        return construireJSONListe(liste_livres);
    }   
    
    private String construireJSONListe(List<Livre> ALivres){
        if(ALivres.isEmpty()){
            return "{}";
        }
         //String collection="{\"livres\":[ ";
         String collection="[ ";
        for(int i=0; i<ALivres.size();i++){
            Livre l = ALivres.get(i);
            collection+=l.json();
            if(i!=ALivres.size()-1){
                collection+=", "; 
            }
        }
        collection+="]"; 
        return collection;
    }
}
